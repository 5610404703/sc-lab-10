package q6;

import java.awt.BorderLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import q7.BankAccount;

public class q6Frame extends JFrame {
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 157;
	
	private static final double DEFAULT_MONEY = 0;
	private static final double INITIAL_BALANCE = 1000; 

	private JLabel moneyLabel, balanceLabel;
	private JTextField moneyField;
	private JButton withdrawButton, depositButton;
	private JPanel bankPanel, balancePanel;
	private BankAccount account;

	
	public q6Frame() {
		account = new BankAccount(INITIAL_BALANCE);
		createTextField();
		createButton();
 		createPanel();
 		
 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}

 	private void createTextField(){
 		moneyLabel = new JLabel("Money(Bath): ");

		final int FIELD_WIDTH = 10;
		moneyField = new JTextField(FIELD_WIDTH);
		moneyField.setText("" + DEFAULT_MONEY);
 	}
 	
 	private void createButton(){
 		withdrawButton = new JButton("Withdraw");  	
		class AddInterestListenerWithdraw implements ActionListener{
			public void actionPerformed(ActionEvent event){
				account.withdraw(Double.parseDouble(moneyField.getText()));
				balanceLabel.setText("Balance: "+account.getBalance()+ " Bath.");
			} 
		}		
		AddInterestListenerWithdraw listenerWithdraw = new AddInterestListenerWithdraw();
		withdrawButton.addActionListener(listenerWithdraw);
		
		depositButton = new JButton("Deposit");  
		balanceLabel = new JLabel();
		balanceLabel.setText("Balance: "+account.getBalance()+ " Bath.");
		class AddInterestListenerDeposit implements ActionListener{
			public void actionPerformed(ActionEvent event){
				account.deposit(Double.parseDouble(moneyField.getText()));
				balanceLabel.setText("Balance: "+account.getBalance()+ " Bath.");
			} 
		}		
		AddInterestListenerDeposit listenerDeposit = new AddInterestListenerDeposit();
		depositButton.addActionListener(listenerDeposit);
		
 	}
 	 
 	private void createPanel(){
 		bankPanel = new JPanel();
 		bankPanel.setBorder(new TitledBorder(new EtchedBorder(),"Withdraw/Deposit"));
 		bankPanel.add(moneyLabel);
 		bankPanel.add(moneyField);
 		bankPanel.add(withdrawButton);
 		bankPanel.add(depositButton);		
 		bankPanel.add(depositButton);
 		
 		balancePanel = new JPanel();
 		balancePanel.setBorder(new TitledBorder(new EtchedBorder(),"Show Balnce"));
 		balancePanel.add(balanceLabel);
 		add(bankPanel, BorderLayout.NORTH);
 		add(balancePanel, BorderLayout.CENTER);
 	} 
 	
}
