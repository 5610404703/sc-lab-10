package q7;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import q7.BankAccount;

public class q7Frame extends JFrame {
	private static final int FRAME_WIDTH = 600;
	private static final int FRAME_HEIGHT = 380;
	
	private static final double DEFAULT_MONEY = 0;
	private static final double INITIAL_BALANCE = 1000; 
	
	private static final int AREA_ROWS = 15;
	private static final int AREA_COLUMNS = 50;

	private JLabel moneyLabel;
	private JTextField moneyField;
	private JButton buttonWithdraw, buttonDeposit;
	private JPanel bankPanel, balancePanel;
	private JTextArea balanceArea;
	private BankAccount account;

	
	public q7Frame() {
		account = new BankAccount(INITIAL_BALANCE);		
		createTextField();
		createButton();
		balanceArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
		balanceArea.setEditable(false);		
		createPanel();

 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}

 	private void createTextField(){
 		moneyLabel = new JLabel("Money(Bath): ");

		final int FIELD_WIDTH = 10;
		moneyField = new JTextField(FIELD_WIDTH);
		moneyField.setText("" + DEFAULT_MONEY);
 	}
 	
 	private void createButton(){
 		buttonWithdraw = new JButton("Withdraw");  	
		class AddInterestListenerWithdraw implements ActionListener{
			public void actionPerformed(ActionEvent event){
				account.withdraw(Double.parseDouble(moneyField.getText()));
				balanceArea.append("Withdraw: "+moneyField.getText()+", Balance: "+account.getBalance()+ " Bath.\n");
			} 
		}		
		AddInterestListenerWithdraw listenerWithdraw = new AddInterestListenerWithdraw();
		buttonWithdraw.addActionListener(listenerWithdraw);
		
		buttonDeposit = new JButton("Deposit");  
		class AddInterestListenerDeposit implements ActionListener{
			public void actionPerformed(ActionEvent event){
				account.deposit(Double.parseDouble(moneyField.getText()));
				balanceArea.append("Deposit: "+moneyField.getText()+", Balance: "+account.getBalance()+ " Bath.\n");
			} 
		}		
		AddInterestListenerDeposit listenerDeposit = new AddInterestListenerDeposit();
		buttonDeposit.addActionListener(listenerDeposit);
		
 	}
 	 
 	private void createPanel(){
 		bankPanel = new JPanel();
 		bankPanel.setBorder(new TitledBorder(new EtchedBorder(),"Withdraw/Deposit"));
 		bankPanel.add(moneyLabel);
 		bankPanel.add(moneyField);
 		bankPanel.add(buttonWithdraw);
 		bankPanel.add(buttonDeposit);		
 		
 		balancePanel = new JPanel();
 		balancePanel.setBorder(new TitledBorder(new EtchedBorder(),"Show Balnce"));
 		JScrollPane scrollPane = new JScrollPane(balanceArea);
 		balancePanel.add(scrollPane);
 		add(bankPanel, BorderLayout.NORTH);
 		add(balancePanel, BorderLayout.CENTER);
 	} 
 	
}
