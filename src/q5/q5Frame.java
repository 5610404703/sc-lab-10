package q5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class q5Frame extends JFrame {
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	private JPanel checkBoxPanel, bgPanel;
	
	public q5Frame() {		
 		createPanel();

 		JMenuBar menuBar = new JMenuBar(); 
 		setJMenuBar(menuBar);
 		menuBar.add(createFileMenu());
 		menuBar.add(createBackgroundMenu());
 		 
 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
 	}
 	 
 	private void createPanel(){
 		bgPanel = new JPanel();
 		bgPanel.setSize(200,200);
		add(bgPanel, BorderLayout.CENTER);
		
		checkBoxPanel = new JPanel();
		checkBoxPanel.setSize(200,200);

		checkBoxPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));  

		
		add(checkBoxPanel, BorderLayout.SOUTH);	 
 	} 
 	

 	private JMenu createFileMenu(){
 		JMenu menu = new JMenu("File");
 		menu.add(createFileExitItem());
 		return menu;
 	}
 	
 	private JMenuItem createFileExitItem(){
 		JMenuItem item = new JMenuItem("Exit"); 
 		class MenuItemListener implements ActionListener{
 			public void actionPerformed(ActionEvent event){
 				System.exit(0);
 			}
 		} 
 		ActionListener listener = new MenuItemListener();
 		item.addActionListener(listener);
 		return item;
 	}

 	private JMenu createBackgroundMenu(){
 		JMenu menu = new JMenu("Background");
 		menu.add(createFaceMenu());
 		return menu;
 	} 
 	
 	private JMenuItem createBgItem(final String color){
 		JMenuItem item = new JMenuItem(color); 
 		class MenuItemListener implements ActionListener{
 			public void actionPerformed(ActionEvent event){
 				if(color.equals("Red")){
 					bgPanel.setBackground(Color.RED);
 				}
 				else if(color.equals("Green")){
 					bgPanel.setBackground(Color.GREEN);
 				}
 				else if(color.equals("Blue")){
 					bgPanel.setBackground(Color.BLUE);
 				}
 			}
 		} 
	 	ActionListener listener = new MenuItemListener();
	 	item.addActionListener(listener);
	 	return item;
 	}

 	public JMenu createFaceMenu(){
 		JMenu menu = new JMenu("Color");
 		menu.add(createBgItem("Red"));
 		menu.add(createBgItem("Green"));
 		menu.add(createBgItem("Blue"));
 		return menu;
 	} 
 	
}
