package q1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class q1Frame extends JFrame {
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JButton redButton, greenButton, blueButton ;
	private JPanel buttonPanel, bgPanel;
	
	public q1Frame() {		
		createRadioButton();
 		createPanel();
 		
 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
 	
 	private void createRadioButton(){
 		redButton = new JButton("Red");
 		class AddInterestListenerRed implements ActionListener{
			public void actionPerformed(ActionEvent event){
				bgPanel.setBackground(Color.RED);	
			} 
		}
 		AddInterestListenerRed listenerRed = new AddInterestListenerRed();
		redButton.addActionListener(listenerRed);
		
		greenButton = new JButton("Green");
 		class AddInterestListenerGreen implements ActionListener{
			public void actionPerformed(ActionEvent event){
				bgPanel.setBackground(Color.GREEN);	
			} 
		}
 		AddInterestListenerGreen listenerGreen = new AddInterestListenerGreen();
		greenButton.addActionListener(listenerGreen);
		
		blueButton = new JButton("Blue");
 		class AddInterestListenerBlue implements ActionListener{
			public void actionPerformed(ActionEvent event){
				bgPanel.setBackground(Color.BLUE);	
			} 
		}
 		AddInterestListenerBlue listenerBlue = new AddInterestListenerBlue();
		blueButton.addActionListener(listenerBlue);

 	}
 	 
 	private void createPanel(){
 		bgPanel = new JPanel();
 		bgPanel.setSize(200,200);
		add(bgPanel, BorderLayout.CENTER);
		
		buttonPanel = new JPanel();
		buttonPanel.setSize(200,200);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));        
		buttonPanel.add(redButton);
		buttonPanel.add(greenButton);
		buttonPanel.add(blueButton); 
		buttonPanel.setBorder(new TitledBorder(new EtchedBorder(),"Background Color"));
		
		add(buttonPanel, BorderLayout.SOUTH);		     
 	} 
 	
}
