package q2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class q2Frame extends JFrame {
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JRadioButton redButton, greenButton, blueButton ;
	private JPanel radioPanel, bgPanel;
	
	public q2Frame() {		
		createRadioButton();
 		createPanel();
 		
 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
 	
 	private void createRadioButton(){
 		redButton = new JRadioButton("Red");
 		greenButton = new JRadioButton("Green");
 		blueButton = new JRadioButton("Blue");
 		
 		ButtonGroup group = new ButtonGroup(); 
		group.add(redButton);
		group.add(greenButton);
		group.add(blueButton);
		
		class AddInterestListenerRed implements ActionListener{
			public void actionPerformed(ActionEvent event){
				if(redButton.isSelected()){
					bgPanel.setBackground(Color.RED);
				}
				else if(greenButton.isSelected()){
					bgPanel.setBackground(Color.GREEN);
				}
				else{
					bgPanel.setBackground(Color.BLUE);
				}				
			} 
		}
 		ActionListener listener = new AddInterestListenerRed();
 		redButton.addActionListener(listener);
 		greenButton.addActionListener(listener);
 		blueButton.addActionListener(listener);
 	}
 	 
 	private void createPanel(){
 		bgPanel = new JPanel();
 		bgPanel.setSize(200,200);
		add(bgPanel, BorderLayout.CENTER);
		
		radioPanel = new JPanel();
		radioPanel.setSize(200,200);

		radioPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));        
		radioPanel.add(redButton);
		radioPanel.add(greenButton);
		radioPanel.add(blueButton);
		radioPanel.setBorder(new TitledBorder(new EtchedBorder(),"Background Color"));
		
		add(radioPanel, BorderLayout.SOUTH);	     
 	} 
 	
}
