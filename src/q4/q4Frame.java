package q4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class q4Frame extends JFrame {
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JComboBox<String> colorComboBox;
	private JPanel radioPanel, bgPanel;
	
	public q4Frame() {		
		createComboBox();
 		createPanel();
 		
 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
 	
 	private void createComboBox(){
 		colorComboBox = new JComboBox<String>();
 		colorComboBox.addItem("(None)");
 		colorComboBox.addItem("Red");
 		colorComboBox.addItem("Green");
 		colorComboBox.addItem("Blue");
		
		class AddInterestListenerRed implements ActionListener{
			public void actionPerformed(ActionEvent event){
				bgPanel.setVisible(true);
				if(colorComboBox.getSelectedItem().equals("(None)")){
					bgPanel.setVisible(false);
				}
				else if(colorComboBox.getSelectedItem().equals("Red")){
					bgPanel.setBackground(Color.RED);
				}
				else if(colorComboBox.getSelectedItem().equals("Green")){
					bgPanel.setBackground(Color.GREEN);
				}
				else{
					bgPanel.setBackground(Color.BLUE);
				}
			} 
		}
 		ActionListener listener = new AddInterestListenerRed();
 		colorComboBox.addActionListener(listener);
 	}
 	 
 	private void createPanel(){
 		bgPanel = new JPanel();
 		bgPanel.setSize(200,200);
		add(bgPanel, BorderLayout.CENTER);
		
		radioPanel = new JPanel();
		radioPanel.setSize(200,200);

		radioPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));          		
		radioPanel.add(colorComboBox);
		radioPanel.setBorder(new TitledBorder(new EtchedBorder(),"Background Color"));
		
		add(radioPanel, BorderLayout.SOUTH);	     
 	} 
 	
}
