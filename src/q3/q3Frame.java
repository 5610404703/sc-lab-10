package q3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class q3Frame extends JFrame {
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 500;
	
	private JCheckBox redCheckBox, greenCheckBox, blueCheckBox ;
	private JPanel checkBoxPanel, bgPanel;
	private ActionListener listener;
	
	public q3Frame() {		
		createCheckBox();
 		createPanel();
 		
 		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
 	
 	private void createCheckBox(){
 		redCheckBox = new JCheckBox("Red");
 		greenCheckBox = new JCheckBox("Green");
 		blueCheckBox = new JCheckBox("Blue"); 		
 		
 		class ChoiceListener implements ActionListener{ 
 			public void actionPerformed(ActionEvent event){
 				bgPanel.setVisible(true);
 				if(redCheckBox.isSelected() && greenCheckBox.isSelected() && blueCheckBox.isSelected()){
 					bgPanel.setBackground(Color.BLACK);
 				} 				
 				else if(redCheckBox.isSelected() && greenCheckBox.isSelected()){
 					bgPanel.setBackground(Color.YELLOW);
 				}
 				else if(redCheckBox.isSelected() && blueCheckBox.isSelected()){
 					bgPanel.setBackground(Color.MAGENTA);
 				}
 				else if(greenCheckBox.isSelected() && blueCheckBox.isSelected()){
 					bgPanel.setBackground(Color.CYAN);
 				}
 				else if(redCheckBox.isSelected()){
 					bgPanel.setBackground(Color.RED);
 				}
 				else if(greenCheckBox.isSelected()){
 					bgPanel.setBackground(Color.GREEN);
 				}	
 				else if(blueCheckBox.isSelected()){
 					bgPanel.setBackground(Color.BLUE);
 				}	
 				else{
 					bgPanel.setVisible(false);
 				}
 			}
 		}		 
 		listener = new ChoiceListener();
 		redCheckBox.addActionListener(listener);
 		greenCheckBox.addActionListener(listener);
 		blueCheckBox.addActionListener(listener);
 	}
 	 
 	private void createPanel(){
 		bgPanel = new JPanel();
 		bgPanel.setSize(200,200);
		add(bgPanel, BorderLayout.CENTER);
		
		checkBoxPanel = new JPanel();
		checkBoxPanel.setSize(200,200);

		checkBoxPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		checkBoxPanel.setBorder(new TitledBorder(new EtchedBorder(),"Background Color"));
		checkBoxPanel.add(redCheckBox);
		checkBoxPanel.add(greenCheckBox);
		checkBoxPanel.add(blueCheckBox);  
		
		add(checkBoxPanel, BorderLayout.SOUTH);	 
		
 	} 
 	
}
